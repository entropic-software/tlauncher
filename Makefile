APP_NAME = tlauncher

VERSION:=$(shell grep ^version: shard.yml | cut -f 2 -d : | tr -d ' ')
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

SRCS = $(wildcard src/*.cr)

bin/$(APP_NAME): lib $(SRCS)
	shards build

release: lib $(SRCS)
	shards build --release --link-flags=-s


# Static linking does not work. It seems Gtk3 cannot be linked statically(?)
# static: lib $(SRCS)
# 	mkdir -p bin
# 	podman build -t $(APP_NAME) .
# 	podman run --rm -it -v $(PWD):/src --name $(APP_NAME) localhost/$(APP_NAME) shards build --static

# static-release: lib $(SRCS)
# 	mkdir -p bin
# 	podman build -t $(APP_NAME) .
# 	podman run --rm -it -v $(PWD):/src --name $(APP_NAME) localhost/$(APP_NAME) shards build --static --release --link-flags=-s


lib: shard.yml
	shards install

test:
	crystal spec

run: bin/$(APP_NAME)
	./bin/$(APP_NAME)

debug: lib $(SRCS)
	shards build --debug
	lldb ./bin/$(APP_NAME)

gtk-debug: bin/$(APP_NAME)
	GTK_DEBUG=interactive ./bin/$(APP_NAME)

clean: deb-clean
	rm -rf bin

dist-clean: clean
	rm lib -rf


# Release build for different systems

docker-alpine-release: lib test $(SRCS)
	mkdir -p bin
	podman build -t $(APP_NAME)-alpine -f docker/Dockerfile.alpine .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME)-alpine localhost/$(APP_NAME)-alpine shards build --release --link-flags=-s

docker-debian-bookworm-release: lib test $(SRCS)
	mkdir -p bin
	podman build -t $(APP_NAME)-debian-bookworm -f docker/Dockerfile.debian-bookworm .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME)-debian-bookworm localhost/$(APP_NAME)-debian-bookworm shards build --release --link-flags=-s

docker-debian-bullseye-release: lib test $(SRCS)
	mkdir -p bin
	podman build -t $(APP_NAME)-debian-bullseye -f docker/Dockerfile.debian-bullseye .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME)-debian-bullseye localhost/$(APP_NAME)-debian-bullseye shards build --release --link-flags=-s

docker-ubuntu-release: lib test $(SRCS)
	mkdir -p bin
	podman build -t $(APP_NAME)-ubuntu -f docker/Dockerfile.ubuntu .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME)-ubuntu localhost/$(APP_NAME)-ubuntu shards build --release --link-flags=-s


# Debian packaging

DEB_VERSION = $(VERSION)-$(SHORT_DATE)
PKG_NAME = tlauncher
DEB_PKG_BULLSEYE = $(PKG_NAME)_$(DEB_VERSION)~bullseye_amd64.deb
DEB_PKG_BOOKWORM = $(PKG_NAME)_$(DEB_VERSION)~bookworm_amd64.deb

deb-pkg-bullseye: docker-debian-bullseye-release
	./mkdebpkg.sh bullseye $(DEB_VERSION)
	mv debian/build/bullseye/$(PKG_NAME).deb debian/build/$(DEB_PKG_BULLSEYE)

deb-pkg-bookworm: docker-debian-bookworm-release
	./mkdebpkg.sh bookworm $(DEB_VERSION)
	mv debian/build/bookworm/$(PKG_NAME).deb debian/build/$(DEB_PKG_BOOKWORM)


deb-repo-bullseye: deb-pkg-bullseye
	update-deb-repo.sh -f $(PWD)/debian/build/$(DEB_PKG_BULLSEYE) -r bullseye

deb-repo-bookworm: deb-pkg-bookworm
	update-deb-repo.sh -f $(PWD)/debian/build/$(DEB_PKG_BOOKWORM) -r bookworm

deb-repo: deb-repo-bookworm deb-repo-bullseye


deb-clean:
	rm -rf debian/build
