macro macro_read_version
  {{
    read_file("#{__DIR__}/../shard.yml").split("\n").select { |line|
      line.starts_with? "version:"
    }.first.split(":").last.strip
  }}
end

macro macro_read_git_rev
  {{ system("git rev-parse --short HEAD || echo 'unknown'").stringify }}
end

macro macro_compile_date
  {{ system(%{LC_TIME=C date +"%F %T %z" || echo "unknown"}).stringify }}
end

MyApp::NAME = "tlauncher"
MyApp::VERSION = macro_read_version
MyApp::VERSION_GIT = macro_read_git_rev
MyApp::COMPILE_DATE = macro_compile_date
MyApp::COPYRIGHT_YEAR = "2023"
