require "sqlite3"

class ConfigurationError < Exception
end

alias ScoreValue = Int64
alias Scores = Hash(String, ScoreValue)

class History
  private property conn : DB::Database
  private property db_file : String

  def initialize
    if !ENV.has_key? "HOME"
      raise ConfigurationError.new "Could not determine home directory"
    end

    @db_file = ENV["HOME"] + "/.local/share/tlauncher/history.sqlite"

    Dir.mkdir_p(File.dirname(@db_file))
    @conn = DB.open("sqlite3://#{@db_file}")
    @conn.exec "CREATE TABLE IF NOT EXISTS history (file text, score int, last_used text)"
  end

  def scores(files : Array(String)) : Scores
    in_frag = files.map { |e| "?" }.join(",")
    q = "SELECT file,score FROM history WHERE file IN (#{in_frag})"

    scores = Scores.new
    @conn.query(q, args: files) do |res|
      res.each do
        file = res.read(String)
        score = res.read(ScoreValue)
        scores[file] = score
      end
    end

    return scores
  end

  def update(file : String)
    # TODO: remove entries older than x days?

    begin
      q = "SELECT score FROM history WHERE file = ?"
      score : ScoreValue = @conn.query_one(q, file, as: ScoreValue)

      new_score = score + 1
      @conn.exec(
        %{UPDATE history
        SET
          score = ?,
          last_used = datetime("now")
        WHERE file = ?},
        new_score, file
      )
    rescue e : DB::NoResultsError
      @conn.exec(
        %{INSERT INTO history VALUES (?, 1, datetime("now"))},
        file
      )
    end
  end
end
