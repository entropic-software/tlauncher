require "./history.cr"

record SearchResultItem,
  name : String,
  comment : String,
  file : String,
  icon : String,
  score : Int32 do
end

alias SearchResults = Array(SearchResultItem)

class SearchEngine
  MAX_ITEMS = 20

  # private property locale_lang : String = ""
  private property history : History
  private property path_list : Array(String) = ["/usr/bin", "/bin"]
  private property desktop_file_globs : Array(String) = [
    "/usr/share/applications/*.desktop",
    "/usr/local/share/applications/*.desktop",
    "/usr/share/applications/kde4/*.desktop",
  ]

  def initialize(@history : History)
    if ENV.has_key? "PATH"
      @path_list = ENV["PATH"].split(":")
    end

    # TODO: also match name/comment in locale language?
    # if ENV.has_key? "LANG"
    #   @locale_lang = ENV["LANG"].split("_").first
    # end

    if ENV.has_key? "HOME"
      homedir = ENV["HOME"]
      @desktop_file_globs << "#{homedir}/.local/share/applications/*.desktop"
    end
  end

  def find(query : String) : SearchResults
    results = [] of SearchResultItem

    query = Regex.escape(query)
    return results if query.size < 2

    desktop_files = find_desktop_files(query)
    executables = find_executables(query)
    results = executables + desktop_files

    # Add scores from history
    scores_from_history = @history.scores(results.map { |e| e.file })
    results = results.map { |e|
      if scores_from_history.has_key? e.file
        SearchResultItem.new(
          name: e.name,
          file: e.file,
          comment: e.comment,
          icon: e.icon,
          score: e.score + scores_from_history[e.file]
        )
      else
        e
      end
    }

    # TODO: remove duplicates between desktop_files and executables?

    return results.sort { |a, b| b.score <=> a.score }.truncate(0..MAX_ITEMS)
  end

  private def find_executables(query) : SearchResults
    results = [] of SearchResultItem

    @path_list.each do |path|
      Dir.glob("#{path}/*#{query}*").each do |filename|
        if File.executable?(filename) && File.file?(filename)
          basename = File.basename(filename)
          results << SearchResultItem.new(
            name: basename,
            file: filename,
            comment: "",
            icon: "",
            score: score_for_exe_file(basename, query)
          )
        end
      end
    end

    return results
  end

  private def find_desktop_files(query) : SearchResults
    matching_files = [] of String
    regex1 = /^Type=Application$/im
    # Name/Comment matches anywhere.
    # Exec matches only in the main command, not any arguments.
    regex2 = /^((Name|Comment)=.*#{query}.*|Exec=[^ ]*#{query}.*)/im

    @desktop_file_globs.each do |pattern|
      Dir.glob(pattern).each do |filename|
        File.open(filename) do |file|
          data = file.gets_to_end
          matching_files << filename if regex1.match(data) && regex2.match(data)
        end
      end
    end

    results = [] of SearchResultItem
    matching_files.each do |filename|
      begin
        dt_entry = DesktopEntry.new filename
        next if dt_entry.hidden?

        score : Int32 = 0
        found_name = false
        found_exec = false
        found_comment = false

        _score = score_for_desktop_name(dt_entry.name, query)
        if _score > 0
          found_name = true
          score += _score
        end

        _score = score_for_desktop_exec(dt_entry.exec, query)
        if _score > 0
          found_exec = true
          score += _score
        end

        _score = score_for_desktop_comment(dt_entry.comment, query)
        if _score > 0
          found_comment = true
          score += _score
        end

        next if score < 1

        results << SearchResultItem.new(
          name: dt_entry.name,
          file: filename,
          comment: dt_entry.comment,
          icon: dt_entry.icon,
          score: score
        )
      rescue idee : InvalidDesktopEntryError
        STDERR.puts "Warning: Invalid desktop entry (or a bug): #{filename}"
      end
    end

    return results
  end

  private def score_for_exe_file(value : String, query : String) : Int32
    if /^#{query}$/i =~ value
      # Matches exactly
      5
    elsif /^#{query}.*$/i =~ value
      # Matches beginning
      3
    elsif /^.*#{query}.*$/i =~ value
      # Matches anywhere
      1
    else
      0
    end
  end

  private def score_for_desktop_name(value : String, query : String) : Int32
    if /^#{query}$/i =~ value
      21
    elsif /^#{query}.*$/i =~ value
      11
    elsif /^(.* |)#{query}(.* |)$/i =~ value
      # Matches somewhere, whole word
      8
    elsif /^.*#{query}.*$/i =~ value
      # Matches somewhere, part of word
      6
    else
      0
    end
  end

  private def score_for_desktop_exec(value : String, query : String) : Int32
    if /^#{query}( .*|)$/i =~ value
      # Matches entirely, incl path
      10
    elsif /^.*\/#{query}.*$/i =~ value
      # Matches, excl path
      8
    elsif /^.*#{query}.*$/i =~ value
      # Matches somewhere, part of word
      1
    else
      0
    end
  end

  private def score_for_desktop_comment(value : String, query : String) : Int32
    if /^(.* |)#{query}(.* |)$/i =~ value
      # Matches somewhere, whole word
      2
    elsif /^.*#{query}.*$/i =~ value
      # Matches somewhere, part of word
      1
    else
      0
    end
  end
end
