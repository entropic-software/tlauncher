# ref: https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html

require "ini"

class InvalidDesktopEntryError < Exception
end

class HiddenDesktopEntryError < Exception
end

class DesktopEntry
  private property data : Hash(String, String) = {} of String => String

  def initialize(filename)
    ini = INI.parse(File.read(filename))
    begin
      @data = ini["Desktop Entry"]
    rescue ke : KeyError
      raise InvalidDesktopEntryError.new
    end
  end

  def hidden?
    if @data.has_key? "Hidden"
      return true if @data["Hidden"] == "true"
    elsif @data.has_key? "NoDisplay"
      return true if @data["NoDisplay"] == "true"
    end
    return false
  end

  def [](key : String) : String
    begin
      @data[key]
    rescue ke : KeyError
      ""
    end
  end

  def comment
    self["Comment"]
  end

  def exec
    # Remove percent formatting codes from command
    self["Exec"].gsub(/ %./) { "" }.strip
  end

  def name
    self["Name"]
  end

  def icon
    self["Icon"]
  end
end
