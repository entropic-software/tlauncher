require "gobject/gtk/autorun"
require "gobject/g_lib"
require "option_parser"

require "./desktop_file.cr"
require "./search_engine.cr"
require "./version_info.cr"

OptionParser.parse do |parser|
  parser.banner = "Usage: #{MyApp::NAME} [arguments]\n" +
                  "\tGtk-based application launcher"

  parser.on("-h", "--help", "Show this help") do
    STDERR.puts parser
    exit
  end

  parser.on("-V", "--version", "Show version information") do
    STDERR.puts "#{MyApp::NAME} version #{MyApp::VERSION}, rev #{MyApp::VERSION_GIT}"
    STDERR.puts "Compiled at #{MyApp::COMPILE_DATE}"
    STDERR.puts "Copyright (c) #{MyApp::COPYRIGHT_YEAR} Tomas Åkesson"
    exit
  end

  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit 1
  end
end

class ResultsList
  enum Column : Int32
    ICON
    NAME
    FILE
  end

  # The types of the above `enum Column`
  COLUMN_TYPES = [
    GObject::Type::UTF8,
    GObject::Type::UTF8,
    GObject::Type::UTF8,
  ]

  property model : Gtk::TreeModel
  property store : Gtk::ListStore
  property view : Gtk::TreeView
  property scroll_view : Gtk::ScrolledWindow

  def initialize
    @store = Gtk::ListStore.new(COLUMN_TYPES)
    @view = Gtk::TreeView.new_with_model(@store)
    @view.headers_visible = false
    @view.search_column = -1 # Disable search popup
    @model = Gtk::TreeModel.cast(@view.model)

    icon_renderer = Gtk::CellRendererPixbuf.new
    icon_column = Gtk::TreeViewColumn.new
    icon_column.pack_start(icon_renderer, expand: false)
    icon_column.add_attribute(icon_renderer, "icon-name", Column::ICON.value)
    @view.append_column(icon_column)

    text_renderer = Gtk::CellRendererText.new
    name_column = Gtk::TreeViewColumn.new
    name_column.pack_start(text_renderer, expand: true)
    name_column.add_attribute(text_renderer, "text", Column::NAME.value)
    @view.append_column(name_column)

    @scroll_view = Gtk::ScrolledWindow.new
    @scroll_view.shadow_type = Gtk::ShadowType::ETCHED_IN
    @scroll_view.set_policy(Gtk::PolicyType::EXTERNAL, Gtk::PolicyType::AUTOMATIC)

    @scroll_view.add(@view)
  end

  def update(search_results : SearchResults)
    @store.clear

    col_idxs = [
      Column::ICON,
      Column::NAME,
      Column::FILE,
    ]

    iterator = Gtk::TreeIter.new
    search_results.each_with_index do |item, idx|
      line2 : String = item.comment.empty? ? item.file : item.comment
      col_values = [
        item.icon,
        "#{item.name}\n#{line2}",
        item.file,
      ]
      @store.append(iterator)
      @store.set(iterator, col_idxs, col_values)
    end

    select_first_row
  end

  def select_first_row
    iter : Gtk::TreeIter = Gtk::TreeIter.new
    if @model.iter_first(iter)
      path = @model.path(iter)
      @view.set_cursor(path, nil, false)
    end
  end

  def grab_focus
    @view.grab_focus
  end

  def has_focus?
    @view.has_focus
  end
end

class SearchInput
  property entry : Gtk::Entry

  def initialize
    @entry = Gtk::Entry.new
  end

  def grab_focus
    @entry.grab_focus_without_selecting
  end

  def has_focus?
    @entry.has_focus
  end

  def text
    @entry.text
  end
end

class Window
  private property search_input : SearchInput
  private property results_list : ResultsList
  private property search_engine : SearchEngine
  private property history : History
  private property window : Gtk::Window
  private property search_timer : UInt32 = 0

  def on_search_changed(widget : Gtk::Editable)
    # Use a timer to prevent updating the list on every 'changed' event
    if @search_timer != 0
      GLib.source_remove(@search_timer)
    end

    @search_timer = GLib.timeout_milliseconds(200) do
      update_list
      @search_timer = 0
      false # Return false because this is a one-shot timer
    end
  end

  def update_list
    @results_list.update(@search_engine.find(@search_input.text))
  end

  def on_return_pressed
    launch_selected
  end

  def on_backspace_pressed
    @search_input.grab_focus if @results_list.has_focus?
    return false # This is needed so backspace can be actually used to remove text
  end

  def on_pgdn_pressed
    @results_list.grab_focus if @search_input.has_focus?
    return false # This is needed so the key press is not swallowed
  end

  private def launch_selected
    iter : Gtk::TreeIter = Gtk::TreeIter.new

    if @results_list.view.selection.selected(nil, iter)
      file : String = @results_list.model.value(iter, ResultsList::Column::FILE.value).as_s

      if /.*\.desktop$/ =~ file
        dt_entry = DesktopEntry.new file
        command_line = dt_entry.exec.strip
        STDERR.puts("launching \"#{command_line}\" from #{file}")
      else
        command_line = file.strip
        STDERR.puts("launching #{file}")
      end

      begin
        Process.new(
          command_line.split(" ").first,
          command_line.split(" ").reject { |e| e.empty? }[1..]?,
          nil, false, false,
          Process::Redirect::Close,  # input
          Process::Redirect::Close,  # output
          Process::Redirect::Inherit # error
        )
      rescue IO::Error
        STDERR.puts "...failed"
      else
        @history.update(file)
        Gtk.main_quit
      end

      # TODO: This crashes for some reason. We don't need it, but something
      # else might be wrong.
      # selected = SearchResultItem.new(
      #   name: "aoe",
      #   comment: "eoeu",
      #   file: "aeo",
      #   icon: "eoue",
      #   score: 0
      # )
      # pp selected
    else
      puts "no selected"
    end
  end

  def initialize
    @window = Gtk::Window.new(default_width: 400, default_height: 300)
    @window.title = "Run Application"
    @window.border_width = 0

    @window.connect("destroy", &->Gtk.main_quit)

    vbox = Gtk::Box.new :vertical, spacing: 0

    @search_input = SearchInput.new
    vbox.add(@search_input.entry)

    @results_list = ResultsList.new
    vbox.pack_start(@results_list.scroll_view, expand: true, fill: true, padding: 0)

    @window.add vbox

    @history = History.new
    @search_engine = SearchEngine.new(@history)
  end

  def setup_signals
    @search_input.entry.on_changed(&->on_search_changed(Gtk::Editable))

    # ref: /usr/include/gtk-3.0/gdk/gdkkeysyms.h
    accel_group = Gtk::AccelGroup.new
    accel_group.connect(Gdk::KEY_Escape, :zero_none, :zero_none, &->Gtk.main_quit)
    accel_group.connect(Gdk::KEY_Return, :zero_none, :zero_none, &->self.on_return_pressed)
    accel_group.connect(Gdk::KEY_BackSpace, :zero_none, :zero_none, &->self.on_backspace_pressed)
    accel_group.connect(Gdk::KEY_Page_Down, :zero_none, :zero_none, &->self.on_pgdn_pressed)
    @window.add_accel_group(accel_group)
  end

  def show
    @window.show_all
  end
end

begin
  w = Window.new
  w.setup_signals
  w.show
rescue e
  puts e
  exit 1
end
