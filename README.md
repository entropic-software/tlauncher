# tlauncher

An application launcher for Unix-like systems. Aimed mainly at people who
just runs a window manager without a full desktop environment.

Will search for desktop-files (primarily) and executables in your $PATH
(secondarily), and sort them according to black magick.

## Screenshot

![Screenshot](screenshot.png)

## Building

Dependencies for building:

[Crystal](https://crystal-lang.org)

### Debian / Ubuntu
- libgtk-3-dev
- libgirepository1.0-dev (see [gobject-introspection for Crystal](https://github.com/jhass/crystal-gobject) for details)
- libsqlite3-dev

### Alpine
- gtk+3.0-dev
- gobject-introspection-dev (see [gobject-introspection for Crystal](https://github.com/jhass/crystal-gobject) for details)
- sqlite-dev

Run `make` or `make release` to build the binary.

Or build with Podman: Run `make docker-X-release`.
See Makefile for supported distributions.

Or build a Debian package with `make deb-pkg-X` (also uses Podman).
See Makefile for supported distributions.

## Installation

Copy `bin/tlauncher` to a directory in your path, like `$HOME/.local/bin`
(for the current user) or `/usr/local/bin` (for system-wide use).

## Contributors

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/tlauncher
