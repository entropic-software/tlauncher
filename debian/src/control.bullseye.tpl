Package: tlauncher
Version: {VERSION}~bullseye
Section: x11
Priority: optional
Architecture: amd64
Depends: libgtk-3-0, libsqlite3-0
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/tlauncher
Description: An application launcher for Unix-like systems.
 Aimed mainly at people who just runs a window manager without a full desktop
 environment.

